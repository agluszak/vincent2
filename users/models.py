from django.contrib.auth.models import AbstractUser

from django.db import models
from django.urls import reverse


class CustomUser(AbstractUser):
    age = models.PositiveIntegerField(null=True, blank=True)
    about = models.CharField(max_length=255, null=True, blank=True)

    def get_absolute_url(self):
        return reverse('user', args=[str(self.id)])
