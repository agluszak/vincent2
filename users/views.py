from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView

from users.models import CustomUser
from .forms import CustomUserCreationForm, CustomUserUpdateForm


def can_edit_only_oneself(self):
    return self.get_object() == self.request.user


class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'


class EditProfileView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = CustomUser
    form_class = CustomUserUpdateForm
    template_name = 'users/profile_edit.html'
    test_func = can_edit_only_oneself
