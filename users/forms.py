from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm

from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=150, required=True)
    about = forms.CharField(max_length=255)

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = ('first_name', 'last_name', 'username', 'email', 'age', 'about')


class CustomUserUpdateForm(ModelForm):
    email = forms.EmailField(required=True)
    about = forms.CharField(max_length=255, required=False)

    class Meta:
        model = CustomUser
        fields = ('email', 'age', 'about')
