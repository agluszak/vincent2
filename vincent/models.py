from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from django.utils import timezone


class Post(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField(editable=False)
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )

    def save(self, *args, **kwargs):
        now = timezone.now()
        if not self.id:
            self.created = now
        self.modified = now
        return super(Post, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post_detail', args=[str(self.id)])

    class Meta:
        ordering = ['-id']


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    comment = models.CharField(max_length=255)
    created = models.DateTimeField(editable=False)
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )

    def save(self, *args, **kwargs):
        now = timezone.now()
        if not self.id:
            self.created = now
        return super(Comment, self).save(*args, **kwargs)

    def __str__(self):
        return self.comment

    def get_absolute_url(self):
        return reverse('post_detail', args=[str(self.post.id)])

    class Meta:
        ordering = ['-created']
