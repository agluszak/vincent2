from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView
from django.views.generic import TemplateView, DetailView, UpdateView, DeleteView, CreateView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import FormView

from users.models import CustomUser
from vincent.forms import CommentForm
from .models import Post, Comment


def can_edit_only_own_posts(self):
    post = self.get_object()
    return post.author == self.request.user


class HomePageView(TemplateView):
    template_name = 'home.html'


class PostListView(ListView):
    model = Post
    template_name = 'posts/post_list.html'
    paginate_by = 6
    context_object_name = 'posts'


class PostDetailView(DetailView):
    model = Post
    template_name = 'posts/post_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CommentForm()
        return context


class CommentCreateView(SingleObjectMixin, FormView):
    model = Comment
    form_class = CommentForm
    template_name = "posts/post_detail.html"
    post_object = None

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.post_object = get_object_or_404(Post, pk=kwargs['pk'])
        comment = self.form_class(request.POST).save(commit=False)
        comment.post = self.post_object
        comment.author = request.user
        comment.save()
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('post_detail', kwargs={'pk': self.post_object.pk})


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ('title', 'body',)
    template_name = 'posts/post_edit.html'
    test_func = can_edit_only_own_posts


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    template_name = 'posts/post_delete.html'
    success_url = reverse_lazy('post_list')
    test_func = can_edit_only_own_posts


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    template_name = 'posts/post_new.html'
    fields = ('title', 'body',)

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class UserProfileView(ListView):
    model = Post
    template_name = 'users/profile_detail.html'
    paginate_by = 6
    context_object_name = 'posts'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = get_object_or_404(CustomUser, id=self.kwargs['pk'])
        context['profile_user'] = user
        return context

    def get_queryset(self):
        queryset = Post.objects.filter(
            author_id=self.kwargs['pk'],
        )
        return queryset
