from django.urls import path

from .views import HomePageView, PostListView, PostUpdateView, PostDetailView, PostDeleteView, PostCreateView, \
    CommentCreateView, UserProfileView

urlpatterns = [
    # home
    path('', HomePageView.as_view(), name='home'),

    # posts
    path('posts/', PostListView.as_view(), name='post_list'),
    path('posts/new/', PostCreateView.as_view(), name='post_create'),
    path('posts/<int:pk>/',
         PostDetailView.as_view(), name='post_detail'),
    path('posts/<int:pk>/edit/',
         PostUpdateView.as_view(), name='post_edit'),
    path('posts/<int:pk>/delete/',
         PostDeleteView.as_view(), name='post_delete'),
    path('posts/<int:pk>/comment/',
         CommentCreateView.as_view(), name='add_comment'),

    # users
    path('users/<int:pk>/',
         UserProfileView.as_view(), name='user')

]
